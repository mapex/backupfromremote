#!/bin/bash

USER="root"

usage()
{
	echo "usage: $0 -s <hostname> -f <tbsfile> -t <targetbackupdir> [-i <identityfile>] [-w] [-D] [-h] [-P] [-n name] [-u user]"
	echo "	-s ... Source Host"
	echo "	-f ... Path to TBS-File (ex: /root/hdva.tbs)"
	echo "	-t ... Directory where to backup"
	echo "	-w ... Filename with Date"
	echo "	-i ... Path to Identityfile (ex: /root/.ssh/rsa_id)"
	echo "	-h ... This Help"
	echo "	-u ... User for ssh - Default: root"
	echo "	-D ... Debug"
	echo "	-n ... Name for the tar file instead of hostname"
	echo "	-P ... for tar -P -> Absolut, doesn't work on busybox"
	echo "	Produces files like: /<targetbackupdir>/<hostname>-backup[-20131231].tar.gz"
	echo " "
}

checkparam()
{
	if [ -z $SOURCEHOST ] 
	then
		usage
		echo "	Hostname is missing"
		exit 1
	fi

	if [ ! -f "$TBSFILE" ] 
	then
		usage
		echo "	No TBSFILE given"
		exit 1
	fi

	if [ ! -d "$TARGETDIRECTORY" ] 
	then
		usage
		echo "	$TARGETDIRECTORY is not an valid directory"
		exit 1
	fi

	if [ -n "${IDENTITY+1}" ] && [ ! -f "$IDENTITY" ]
	then
		usage
		echo "	$IDENTITY is not valid"
		exit 1
	fi

	if [ $WITHDATE ]
	then
		ACTDATE=$(date +20%y%m%d)
		BACKUPFILE="${FILENAME}-backup-${ACTDATE}.tar.gz"
	else
		BACKUPFILE="${FILENAME}-backup.tar.gz"
	fi

	if [ $DEBUG ]
	then
		echo "Backupfile: ${TARGETDIRECTORY}/${BACKUPFILE}"
	fi
}

createtarcommand()
{	#example: ssh root@hostname "tar -pPczf - /home/mapex" > hostname.tar.gz
	if [ $ABSOLUT ]
	then	
		TARCOMMAND="/bin/tar -Pczf - " 
	else
		TARCOMMAND="/bin/tar -czf - " 
	fi

	for SFILE in $(egrep -v -e '^#|^!' ${TBSFILE})
	do
		TARCOMMAND+="${SFILE} "
	done

	for SFILE in $(egrep -e '^!' ${TBSFILE})
	do
		TARCOMMAND+="--exclude=${SFILE:1} "
	done

}


backupfromremote()
{
	createtarcommand
	SSHCOMMAND="ssh -o batchmode=yes "

	if [ -n "${IDENTITY+1}" ]
	then
		SSHCOMMAND+="-i ${IDENTITY} "
	fi

	SSHCOMMAND+="${USER}@${SOURCEHOST}   \" $TARCOMMAND\"   > ${TARGETDIRECTORY}/${BACKUPFILE}"

	if [ $DEBUG ]
	then
		echo "$SSHCOMMAND"
	fi
	
	eval "$SSHCOMMAND"
	exit $?	
}

while getopts "s:f:t:wDhi:Pu:n:" OPTION
do
	case $OPTION in
		h)
			usage
			exit 1
			;;
		s)
			SOURCEHOST=$OPTARG
			FILENAME=$OPTARG
			;;
		f)
			TBSFILE=$OPTARG
			;;
		t)
			TARGETDIRECTORY=$OPTARG
			;;
		w)	
			WITHDATE=1
			;;
		D)
			DEBUG=1
			;;
		P)
			ABSOLUT=1
			;;
		i)
			IDENTITY=$OPTARG
			;;
		u)
			USER=$OPTARG
			;;
		n)
			FILENAME=$OPTARG
			;;
		?)
			usage
			exit 1
			;;
	esac
done

checkparam
backupfromremote

