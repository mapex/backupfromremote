backupfromremote.sh
===================

Provides a simple tar backup from a remote host. It doesn't need the backup space on the remote host, because the tar output is tunneled over ssh.
It works fine and is tested with BASH and sizes >30GB. I use it to backup some raspberry pi hosts and other Linux Hosts. Hosts with busybox (like dd-wrt and openwrt) also work, but there are some restrictions (see below).

For daily backups set up a cronjob like this:

    0 1 * * * root /home/mapex/backup/backupfromremote.sh -s hostname -f /home/mapex/backup/hostname-tbs -t /home/mapex/backup/hostname -i /home/mapex/.ssh/id_rsa 

Requirements
------------

It only needs a configured ssh connection, on the backup server (the host where this script is started) and the gnu tar command on the other host. Make sure you can logon the other host via ssh without typing your password (tutorial for setting up ssh can be found [here](https://help.ubuntu.com/community/SSH/OpenSSH/Keys)).


Usage
-----

    usage: ./backupfromremote.sh -s <hostname> -f <tbsfile> -t <targetbackupdir> [-i <identityfile>] [-w] [-D] [-h] [-P] [-n name] [-u user]
            -s ... Source Host
            -f ... Path to TBS-File (ex: /root/hdva.tbs)
            -t ... Directory where to backup
            -w ... Filename with Date
            -i ... Path to Identityfile (ex: /root/.ssh/rsa_id)
            -h ... This Help
            -D ... Debug - Print some additional output
            -P ... for tar -P -> Absolut, doesn't work on busybox
            -n ... Use name instead of hostname for backupfile
            -u ... User for ssh. Default: root
            Produces files like: /<targetbackupdir>/<hostname>-backup[-20131231].tar.gz

### TBS - File

    /home/mapex
    #/etc/ssh
    !/home/mapex/.bashrc
    !/home/mapex/.bash_logout
    /etc/init.d

The tbs file (To Be Saved) contains a list of files which should be in the tar file. In this example the directory `/home/mapex` and `/etc/init.d` are included, the files `/home/mapex/.bashrc` and `/home/mapex/.bash_logout` are excluded. The line with `#/etc/ssh` is a comment.

On busybox hosts lines with `!/dir/file` won't work and will throw an error, because busybox tar doesn't know the `--exclude` option.

Logrotate
---------

If you don't use the `-w` option, the backup file will be overwritten every day. You can use `logrotate` for creating rotating backups. The example below shows a rotating backup for the last 5 days.

### logrotate.conf

    /path/to/backup/hostname-backup.tar.gz {
        daily
        rotate 5
        nocompress
        dateext
        dateformat -%Y%m%d
        extension .tar.gz
        missingok
    }

### The filename with date option

If you use `-w` option, you can create a simple cronjob which deletes all backup files older than 5 days. See example below.

    0 2 * * * root find /path/to/backup -iname '/path/to/backup/hostname-backup*.tar.gz' -mtime +5 -exec rm {} \;

License
-------

This work is licensed under GPL v3. License Text can be found [here](http://www.gnu.org/licenses/gpl-3.0).
